Первый заголовок
===============

Текст первого абзаца.
Посмотрим, что получится после сборки.

Шарповский код - ниже:

Второго уровня заголовок, падаван
---------------------------------

Текст первого абзаца.
Посмотрим, что получится после сборки.

Шарповский код - ниже:
Хук?
Create a directory inside your project to hold your docs::

	using HCS.Api.Connector.WebService.DTO;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.ServiceModel;
	using System.Text;
	using System.Threading.Tasks;

	namespace HCS.Api.Connector.WebService
	{
		public class WebServiceAsyncSingle<TClient, TPortType> : WebServiceAsync<TClient, TPortType>
			where TClient : ClientBase<TPortType>, IAsyncClient
			where TPortType : class
		{
			public WebServiceAsyncSingle(string endpointAddress) : base(endpointAddress)
			{
			}

			protected override WCFResponse<object[]> CreateResponse(IAckResult result, Type TError)
			{
				var retVal = new WCFResponse<object[]>();
				retVal.Items = ConvertErrors( new object[1] { (result.getStateResult as IAckResultSingle).Item }, TError);
				retVal.RequestState = result.getStateResult.RequestState;
				return retVal;
			}
		}
	}
